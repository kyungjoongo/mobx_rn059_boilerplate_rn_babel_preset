import React, {Component} from 'react';
import {Container, Text, Card, Header, Body, Button, Title, CardItem} from 'native-base';
import CounterStore from './CounterStore';
import {observer} from 'mobx-react';
import {View} from 'react-native';

@observer
export default class Counter extends Component {
    constructor() {
        super();
        this.state = {
            isReady: false
        }
    }

    async componentWillMount() {
        this.setState({isReady: true});
    }

    render() {
        if (!this.state.isReady) {
            return;
        }
        return (




            <Container>
                <Header>
                    <Body>
                    <Title>Mobx Counter</Title>
                    </Body>
                </Header>
                <Card>
                    <CardItem>
                        <Text>
                            {CounterStore.counter}
                        </Text>
                    </CardItem>
                </Card>
                <View style={{height:5}}/>
                <View>
                    <Text>{CounterStore.counter}</Text>
                </View>
                <View style={{height:5}}/>
                <Button primary block onPress={() => CounterStore.increment()}>
                    <Text>Increment</Text>
                </Button>
                <Button primary block onPress={() => CounterStore.decrement()}>
                    <Text>Decrement</Text>
                </Button>

                <Button warning block onPress={() => CounterStore.incrementDouble()}>
                    <Text>INcrement Double</Text>
                </Button>
                <View/>
            </Container>
        );
    }
}