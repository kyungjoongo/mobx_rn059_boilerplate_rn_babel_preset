import {observable} from 'mobx';


class CounterStore {
    @observable counter = 0;

    increment() {
        this.counter++;
        console.log("increment", this.counter);
    }

    decrement() {
        this.counter--;
        console.log("decrement", this.counter);
    }


    incrementDouble() {
        this.counter = this.counter + 2;
        console.log("increment2", this.counter);
    }
}



export default new CounterStore();